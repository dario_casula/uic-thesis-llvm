#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace{
	
	struct Hello_dario: public FunctionPass{
		static char ID;
		
		Hello_dario(): FunctionPass(ID){}
		
		virtual bool runOnFunction(Function &F){ //&F similar to argv
			errs() << "dario Hello_dario: ";
			errs().write_escaped(F.getName()) << " I am Pass\n";
			errs() << F.getName() << "\n"; 
			return false;
		}
	}; //end Hello_dario struct
} //end namespace (anonymous)

char Hello_dario::ID=0;
static RegisterPass<Hello_dario> X("hello_dario","Hello_dario World Pass", false, false);

//===- SimplifyCFGPass.cpp - CFG Simplification Pass ----------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements dead code elimination and basic block merging, along
// with a collection of other peephole control flow optimizations.  For example:
//
//   * Removes basic blocks with no predecessors.
//   * Merges a basic block into its predecessor if there is only one and the
//     predecessor only has one successor.
//   * Eliminates PHI nodes for basic blocks with a single predecessor.
//   * Eliminates a basic block that only contains an unconditional branch.
//   * Changes invoke instructions to nounwind functions to be calls.
//   * Change things like "if (x) if (y)" into "if (x&y)".
//   * etc..
//
//===----------------------------------------------------------------------===//
/*DARIO timing*/ 
#include <sys/time.h>
#include <sys/resource.h>

#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/AssumptionTracker.h"
#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Transforms/Utils/Local.h"
//DARIO
#include "llvm/Support/Debug.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Regex.h"
#include <sstream>

using namespace llvm;

#define DEBUG_TYPE "simplifycfg"

static cl::opt<unsigned>
UserBonusInstThreshold("bonus-inst-threshold", cl::Hidden, cl::init(1),
   cl::desc("Control the number of bonus instructions (default = 1)"));

//DARIO: I added Dario to this first STATISTIC
STATISTIC(NumSimpl, "_____Dario: Number of blocks simplified");
STATISTIC(Dario_NumCall, "_____Dario: CFG Pass - Number of Calls to SimplifyCFGPass");
STATISTIC(Dario_NumUnreachableBlocksRemoved, "&&&&&Dario: CFG Pass - Number of times unreachable blocks are removed");
STATISTIC(Dario_MergeEmptyReturnBlocks, "&&&&&Dario: CFG Pass - Number of return blocks merged");



namespace {
struct CFGSimplifyPass : public FunctionPass {
  static char ID; // Pass identification, replacement for typeid
  unsigned BonusInstThreshold;
  CFGSimplifyPass(int T = -1) : FunctionPass(ID) {
    BonusInstThreshold = (T == -1) ? UserBonusInstThreshold : unsigned(T);
    initializeCFGSimplifyPassPass(*PassRegistry::getPassRegistry());
  }
  bool runOnFunction(Function &F) override;

	/*DARIO begin*/
	bool runOnFunctionDario(Function &F);
	/*DARIO end*/

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.addRequired<AssumptionTracker>();
    AU.addRequired<TargetTransformInfo>();
  }
};
}

char CFGSimplifyPass::ID = 0;
INITIALIZE_PASS_BEGIN(CFGSimplifyPass, "simplifycfg", "Simplify the CFG", false,
                      false)
INITIALIZE_AG_DEPENDENCY(TargetTransformInfo)
INITIALIZE_PASS_DEPENDENCY(AssumptionTracker)
INITIALIZE_PASS_END(CFGSimplifyPass, "simplifycfg", "Simplify the CFG", false,
                    false)

// Public interface to the CFGSimplification pass
FunctionPass *llvm::createCFGSimplificationPass(int Threshold) {
  return new CFGSimplifyPass(Threshold);
}

//###########################################################################################

/*
 * This function returns true if the string passed is a number: Integer or Float(Exponential)
 */
bool isNumberPass(std::string StrToCheck){
	SmallVector<StringRef,2> Matches;
	Matches.push_back("initialization");
	Regex reg("[+-]?[0-9]+([.][0-9]+)?([eE][+-]?[0-9]+)?");
	if(reg.match(StrToCheck,&Matches)){
		if(StrToCheck.size() == (Matches.begin())->size()){
			return true;
		}
		return false;
	}
	return false;
	
}

/*
 * DARIO
 * This function returns the string of the name of the operand without the type information
 */
std::string getFilteredStringPass(Value* Operand, int suffix){
	if(Operand == NULL){
		errs() << "OPERAND IS NULL - getFilteredString\n";
		return ("ERROR"+suffix);
		}
	std::string tmpStr;
	std::string tmpStr2;
	llvm::raw_string_ostream tmpRso(tmpStr);
	llvm::raw_string_ostream tmpRso2(tmpStr2);
	if(dyn_cast<BasicBlock>(Operand)){
		tmpRso << Operand->getName();
		tmpStr = tmpRso.str();
	}else	if(!dyn_cast<Instruction>(Operand)){
		tmpRso << *Operand;
		tmpRso2 << *((*Operand).getType());
		tmpStr = tmpRso.str();
		tmpStr2 = tmpRso2.str();
		tmpStr.erase(0,tmpStr2.size()+1);
		if(Operand->getType()->getTypeID() == Type::HalfTyID ||
										Operand->getType()->getTypeID() == Type::FloatTyID ||
											Operand->getType()->getTypeID() == Type::DoubleTyID){
												
												std::stringstream tmpSs;
												tmpSs << std::fixed << atof(tmpStr.c_str()); //convert scientific notation
												tmpStr2 = tmpSs.str();
												tmpStr.clear();
												tmpStr = tmpStr2;
		}
	}else{
		tmpRso << Operand->getName();
		tmpStr = tmpRso.str();
	}
	if(!isNumberPass(tmpStr)){
		tmpRso2.flush();
		tmpStr2.clear();
		tmpRso2 << "_" << suffix;
		tmpStr2 = tmpRso2.str();
		tmpStr += tmpStr2; 
	}
	return tmpStr;
}
//###########################################################################################


/// mergeEmptyReturnBlocks - If we have more than one empty (other than phi
/// node) return blocks, merge them together to promote recursive block merging.
static bool mergeEmptyReturnBlocks(Function &F) {
  bool Changed = false;

  BasicBlock *RetBlock = nullptr;

	/*DARIO opening files*/
			std::error_code ErrInfoStream;
			std::error_code ErrInfoDecl;
			raw_fd_ostream D_FileStream("/home/dario/witgen_MergeEmptyReturnBlocks", ErrInfoStream, sys::fs::F_Append);
			raw_fd_ostream D_DeclarationStream("/home/dario/declarations_MergeEmptyReturnBlocks", ErrInfoDecl, sys::fs::F_Append);
			
	
	//DARIO end opening files
	
	
  // Scan all the blocks in the function, looking for empty return blocks.
  for (Function::iterator BBI = F.begin(), E = F.end(); BBI != E; ) {
    BasicBlock &BB = *BBI++;

    // Only look at return blocks.
    ReturnInst *Ret = dyn_cast<ReturnInst>(BB.getTerminator());
    if (!Ret) continue;

    // Only look at the block if it is empty or the only other thing in it is a
    // single PHI node that is the operand to the return.
    if (Ret != &BB.front()) {
      // Check for something else in the block.
      BasicBlock::iterator I = Ret;
      --I;
      // Skip over debug info.
      while (isa<DbgInfoIntrinsic>(I) && I != BB.begin())
        --I;
      if (!isa<DbgInfoIntrinsic>(I) &&
          (!isa<PHINode>(I) || I != BB.begin() ||
           Ret->getNumOperands() == 0 ||
           Ret->getOperand(0) != I))
        continue;
    }

    // If this is the first returning block, remember it and keep going.
    if (!RetBlock) {
      RetBlock = &BB;
      continue;
    }

    // Otherwise, we found a duplicate return block.  Merge the two.
    Changed = true;

    // Case when there is no input to the return or when the returned values
    // agree is trivial.  Note that they can't agree if there are phis in the
    // blocks.
    if (Ret->getNumOperands() == 0 ||
        Ret->getOperand(0) ==
          cast<ReturnInst>(RetBlock->getTerminator())->getOperand(0)) {
      BB.replaceAllUsesWith(RetBlock);
			BB.eraseFromParent();
			//DARIO CASE1: if in the function there are at least two empty blocks with return
			//just remove one and use the other instead of the first. 
			//their returns are the same.
      continue;
    }
		
		//DARIO CASE 2
		//the two blocks return different values
		//take the first and put a phi node to decide the value. the second block will unconditionally branch
		//to the first block. remove the return from the second block and return the phi node from the first block.
		/*DARIO case2 witness*/
		
		//initialization
			std::string NumSimplification;
			llvm::raw_string_ostream tmpRso(NumSimplification);
			tmpRso << Dario_MergeEmptyReturnBlocks;
			NumSimplification = tmpRso.str();
			
				D_DeclarationStream << "(echo \"##############MergeEmptyReturnBlocks  number: " << Dario_MergeEmptyReturnBlocks << "\")\n";
				D_FileStream << "(echo \"##############MergeEmptyReturnBlocks  number: " << Dario_MergeEmptyReturnBlocks << "\")\n";
			
				std::string curValue = "value_" + NumSimplification;
				std::string BlockSort = "BLOCK_" + NumSimplification;
				std::string curBlock = "curBlock_"+NumSimplification;
				
				D_DeclarationStream << "(declare-datatypes () ((" << BlockSort << " ";
				D_DeclarationStream << getFilteredStringPass(RetBlock,Dario_MergeEmptyReturnBlocks) << " ";
				D_DeclarationStream << getFilteredStringPass(&BB,Dario_MergeEmptyReturnBlocks) << " ";
				for (pred_iterator D_PI = pred_begin(&BB), E = pred_end(&BB); D_PI != E; ++D_PI) {
					BasicBlock *D_Pred = *D_PI;
					D_DeclarationStream << getFilteredStringPass(D_Pred,Dario_MergeEmptyReturnBlocks) << " ";
				}
				D_DeclarationStream << ")))\n";
				
				D_DeclarationStream << "(declare-const " << curBlock << " " << BlockSort << ")\n";
				
				Value *RetBlockValue = cast<ReturnInst>(RetBlock->getTerminator())->getOperand(0);
				Value *BBValue = cast<ReturnInst>(BB.getTerminator())->getOperand(0);
				
				std::string RetBlockValueString;
				std::string BBValueString;
				
				if(dyn_cast<ConstantInt>(RetBlockValue)){
					D_DeclarationStream << "(declare-const " << curValue << " Bool)\n";	
					if(cast<ConstantInt>(RetBlockValue)->isZero()){
						RetBlockValueString = "false";
					}else{
						RetBlockValueString = "true";
					}
				}else if(dyn_cast<Instruction>(RetBlockValue)){
					D_DeclarationStream << "(declare-const " << curValue << " Int)\n";
					RetBlockValueString = getFilteredStringPass(RetBlockValue,Dario_MergeEmptyReturnBlocks);
					if(!isNumberPass(RetBlockValueString)){
							D_DeclarationStream << "(declare-const " << RetBlockValueString << " Int)\n";
					}
				}
				
				if(dyn_cast<ConstantInt>(BBValue)){
					if(cast<ConstantInt>(BBValue)->isZero()){
						BBValueString = "false";
					}else{
						BBValueString = "true";
					}
				}else if(dyn_cast<Instruction>(BBValue)){
					BBValueString = getFilteredStringPass(BBValue,Dario_MergeEmptyReturnBlocks);
					if(!isNumberPass(BBValueString)){
							D_DeclarationStream << "(declare-const " << BBValueString << " Int)\n";
					}
				}
				
				D_FileStream << "(assert (and ";
				D_FileStream << "(=> (= " << curBlock << " " << getFilteredStringPass(RetBlock,Dario_MergeEmptyReturnBlocks) << ") (= "
										<< curValue << " " << RetBlockValueString << "))";
				D_FileStream << " ";
				D_FileStream << "(=> (= " << curBlock << " " << getFilteredStringPass(&BB,Dario_MergeEmptyReturnBlocks) << ") (= "
										<< curValue << " " << BBValueString << "))";
				D_FileStream << "))\n";
				
		//DARIO end initialization and source part
   

		// If the canonical return block has no PHI node, create one now.
    PHINode *RetBlockPHI = dyn_cast<PHINode>(RetBlock->begin());
    if (!RetBlockPHI) {
      Value *InVal = cast<ReturnInst>(RetBlock->getTerminator())->getOperand(0);
      pred_iterator PB = pred_begin(RetBlock), PE = pred_end(RetBlock);
      RetBlockPHI = PHINode::Create(Ret->getOperand(0)->getType(),
                                    std::distance(PB, PE), "merge",
                                    &RetBlock->front());

      for (pred_iterator PI = PB; PI != PE; ++PI)
        RetBlockPHI->addIncoming(InVal, *PI);
      RetBlock->getTerminator()->setOperand(0, RetBlockPHI);
    }

    // Turn BB into a block that just unconditionally branches to the return
    // block.  This handles the case when the two return blocks have a common
    // predecessor but that return different things.
    RetBlockPHI->addIncoming(Ret->getOperand(0), &BB);
    BB.getTerminator()->eraseFromParent();
    BranchInst::Create(RetBlock, &BB);
		
		/*DARIO target part of the witness*/
		
		for (int D_i = 0; D_i < (int)RetBlockPHI->getNumIncomingValues(); D_i++){
					BasicBlock *D_IncomingPred = RetBlockPHI->getIncomingBlock(D_i);
					Value *D_IncomingValue = RetBlockPHI->getIncomingValue(D_i);
					
					std::string D_IncomingValueString;
					if(dyn_cast<ConstantInt>(D_IncomingValue)){
					if(cast<ConstantInt>(D_IncomingValue)->isZero()){
						D_IncomingValueString = "false";
					}else{
						D_IncomingValueString = "true";
					}
				}else if(dyn_cast<Instruction>(D_IncomingValue)){
					D_IncomingValueString = getFilteredStringPass(D_IncomingValue,Dario_MergeEmptyReturnBlocks);
				}
				
				D_FileStream << "(push)\n";
				D_FileStream << "(assert ";
				D_FileStream << "(=> (= " << curBlock << " " << getFilteredStringPass(D_IncomingPred,Dario_MergeEmptyReturnBlocks) << ") "
												<< "(= " << curValue << " " << D_IncomingValueString << ")))\n";
					D_FileStream << "(check-sat)\n";
					D_FileStream << "(pop)\n";
		}
		
		//DARIO end target part of the witness
		
		
		//DARIO
		if(Changed){
			++Dario_MergeEmptyReturnBlocks;
		}
	
	}
	
	//DARIO closing files
	D_DeclarationStream.flush();
	D_DeclarationStream.close();
	D_FileStream.flush();
	D_FileStream.close();
	
  
	return Changed;
}

/// iterativelySimplifyCFG - Call SimplifyCFG on all the blocks in the function,
/// iterating until no more changes are made.
static bool iterativelySimplifyCFG(Function &F, const TargetTransformInfo &TTI,
                                   const DataLayout *DL,
                                   AssumptionTracker *AT,
                                   unsigned BonusInstThreshold) {
  bool Changed = false;
  bool LocalChange = true;
  while (LocalChange) {
    LocalChange = false;
		
		{//begin dario scope
		
			for (Function::iterator BBIt = F.begin(); BBIt != F.end(); BBIt++) {
			/*DARIO
			 * since values are not required to have a name, we add a random name in case they have not already one
			 * we add name to blocks and instructions inside blocks
			*/
			//THIS IS A BIG CHANGE
			//however, it should not change anything because if there is already a name
				if(!BBIt->hasName()){
					BBIt->setName("d.basicblock");
				}
				for (BasicBlock::iterator i = BBIt->begin(), e = BBIt->end(); i != e; ++i){
					if(!dyn_cast<TerminatorInst>(i) && !i->getType()->isVoidTy()){
								if(!i->hasName()){
									i->setName("d.instruction");
								}
					}
				}
		 }
		
		}//end dario scope
		//DARIO end addition of names

		
    // Loop over all of the basic blocks and remove them if they are unneeded...
    for (Function::iterator BBIt = F.begin(); BBIt != F.end(); ) {
      //DARIO: added statistics into implementation in SimplifyCFG.cpp
			if (SimplifyCFG(BBIt++, TTI, BonusInstThreshold, DL, AT)) {
        LocalChange = true;
        ++NumSimpl;
      }
    }
    Changed |= LocalChange;
	}
  return Changed;
}

/*DARIO*/
/*this function is inserted just to be able to time the simplifyCFG*/
bool CFGSimplifyPass::runOnFunctionDario(Function &F) {
	if (skipOptnoneFunction(F))
    return false;

	Dario_NumCall++;
  
	AssumptionTracker *AT = &getAnalysis<AssumptionTracker>();
  const TargetTransformInfo &TTI = getAnalysis<TargetTransformInfo>();
  DataLayoutPass *DLP = getAnalysisIfAvailable<DataLayoutPass>();
  const DataLayout *DL = DLP ? &DLP->getDataLayout() : nullptr;
	bool EverChanged;
	 
	 EverChanged = removeUnreachableBlocks(F);
	
		if(EverChanged){
			++Dario_NumUnreachableBlocksRemoved;
		}
	
	EverChanged |= mergeEmptyReturnBlocks(F);
	EverChanged |= iterativelySimplifyCFG(F, TTI, DL, AT, BonusInstThreshold);

  // If neither pass changed anything, we're done.
  if (!EverChanged) return false;

  // iterativelySimplifyCFG can (rarely) make some loops dead.  If this happens,
  // removeUnreachableBlocks is needed to nuke them, which means we should
  // iterate between the two optimizations.  We structure the code like this to
  // avoid reruning iterativelySimplifyCFG if the second pass of
  // removeUnreachableBlocks doesn't do anything.

  if (!removeUnreachableBlocks(F)){
		//DARIO: I place curly brackets and the counter
		//this is probably wrong because function removes more than one block at a time
		//see the implementation into Local.cpp
		++Dario_NumUnreachableBlocksRemoved;
    return true;
	}
  do {
    
		EverChanged = iterativelySimplifyCFG(F, TTI, DL, AT, BonusInstThreshold);
		//DARIO
		bool Dario_changed;
		Dario_changed = removeUnreachableBlocks(F);
		if(Dario_changed){
			++Dario_NumUnreachableBlocksRemoved;
		}
		EverChanged |= Dario_changed;
    //EverChanged |= removeUnreachableBlocks(F);
		
 } while (EverChanged);
  

  return true;
	}
/*end function DARIO*/

// It is possible that we may require multiple passes over the code to fully
// simplify the CFG.
//
bool CFGSimplifyPass::runOnFunction(Function &F) {
 
	/*DARIO timing*/
	struct rusage usage1;
  struct timeval start1, end1;

 getrusage(RUSAGE_SELF, &usage1);
  start1 = usage1.ru_utime;
	/*end first part of dario timing*/
	
	bool darioBool = runOnFunctionDario(F);
	
	/*DARIO timing*/
	getrusage(RUSAGE_SELF,& usage1);
  end1 = usage1.ru_utime;
  long startMicrosInSeconds = start1.tv_sec * (__time_t)1000000 + start1.tv_usec;
  long endMicrosInSeconds = end1.tv_sec * (__time_t)1000000 + end1.tv_usec;
  long timeInterval = endMicrosInSeconds - startMicrosInSeconds;
	if(timeInterval>0){
		errs() << "OURSTATS " << timeInterval;
	  errs() << F.getName() << "\n";
	}
 /*DARIO end second part timing*/

  return darioBool;
  
}

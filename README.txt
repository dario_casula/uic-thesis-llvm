Witnessing CFG Optimizations

This repository contains the LLVM code augmented with a CFG witness generator.
[https://bitbucket.org/dario_casula/uic-thesis-llvm/overview]

=========================================================================================================================
Files involved in the simplifications:

SimplifyCFGPAss.cpp
SimplifyCFG.cpp

The generator, for every simplification performed, creates two files composing the witness relation
(make sure to modify in the code the path of the files to be created):

"declarations_[name_of_the_simplification_method]" ==> contains the declaration part of the witness
"witgen_[name_of_the_simplification_method]" ==> contains the witness relation

=========================================================================================================================
To run the generator, simply compile some source code with LLVM. The generator is run together with the SimplifyCFG pass.

example:
	
	"clang -S -emit-llvm test_source.c -o test_source.s" 
		==> compile the source code into llvm IR using clang
	
	"opt -S -mem2reg test_source.s -o test_source_opt.s" 
		==> perform the mem2reg optimization
	
	"opt -S -stats -simplifycfg test_source_opt.s -o test_source_simplifycfg_opt.s" 
		==> perform the SimplifyCFG optimizations
			(the option -stats prints statistic about the simplifications performed)

=========================================================================================================================
To check the witnesses created, merge the two files together and submit to Z3 Microsoft SMT-Solver.
Prerequisite: Microsoft Z3 SMT-solver must be installed

example:
	
	"cat declarations_file witgen_file > complete_file"
		==> merge the two files together (declarations FIRST)

	"z3 complete_file" ==> submit the complete file to z3
